import matplotlib
import omdb
from functions import *

omdbSetup(omdb, 'fe9dd617') # Setup API Key

showkey = input("Enter an IMDB Title Code: ")

# Create series object and display Plot
seriesObj = Series(omdb, showkey)
print("Total Seasons: " + str(seriesObj.totalSeasons))
print("Lowest Rating: " + str(seriesObj.lowestRating))
print("Highest Rating: " + str(seriesObj.highestRating))

# Displays plot
seriesObj.displayPlot(seriesObj.getX(), seriesObj.getY(), seriesObj.getOuterObject(), seriesObj.getEpisodeDict())
